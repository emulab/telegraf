Building From Source
--------------------

Here are some quick instructions for building Linux binary packages
(deb, rpm, tar.gz) on Ubuntu 18.04:

```
apt-get install python git build-essential ruby-dev rpm
gem install fpm
export GOROOT=$HOME/go-bin
mkdir $GOROOT
wget https://dl.google.com/go/go1.11.2.linux-amd64.tar.gz
tar -xzvf go1.11.2.linux-amd64.tar.gz --strip-components=1 -C $GOROOT
export GOPATH=$HOME/go
export PATH=$PATH:$GOROOT/bin:$GOPATH/bin
export OUTPUTDIR=$HOME/output
mkdir -p $OUTPUTDIR
go get -u github.com/golang/dep/cmd/dep
go get -d github.com/influxdata/telegraf
cd $GOPATH/src/github.com/influxdata/telegraf
git remote rename origin upstream
git remote add origin https://gitlab.flux.utah.edu/emulab/telegraf
git checkout origin/master
scripts/build.py --arch=amd64 --platform linux --package -o $OUTPUTDIR
```

The built packages (and the statically-linked `telegraf` binary) will be
in `$OUTPUTDIR`.
